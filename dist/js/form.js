import { Controller, Factory } from "./controller.js";

class FieldController extends Controller
{
	constructor(item)
	{
		super(item);

		this.field = item.closest('.field');

		item.onblur = (e) => this.processRules();
		item.onfocus = (e) => this.reset();

		// Back errors
		let data_errors = item.dataset.errors;
		if(data_errors != undefined)
		{
			var field = item.closest('.field');
			var errors = JSON.parse(item.dataset.errors);
			Object.entries(errors).forEach(([name, message]) =>
			{
				var messageElem = field.querySelector(".error-message");
				messageElem.innerHTML = message;
				if(['captcha', 'equals', 'length'].includes(name))
					field.classList.add("error");
			});
		}
	}

	reset()
	{
		var message = this.field.querySelector(".error-message");
		message.innerHTML = "";
		this.field.classList.remove("error");
	};

	processRules() {
		var rules = JSON.parse(this.item.dataset.rules);

		Object.entries(rules).forEach(([name, params]) => {
			if(name == 'required')
			{
				if(params == true && this.item.value == '')
				{
					var message = this.field.querySelector(".error-message");
					message.innerHTML = "Champ requis";
					this.field.classList.add("error");
				}
			}
			else if(name == 'length')
			{
				var length = params[0];
				if(this.item.value.length < length)
				{
					var message = this.field.querySelector(".error-message");
					message.innerHTML = "Doit faire au moins " + length + " caractères";
					this.field.classList.add("error");
				}
			}
			else if(name == 'equals')
			{
				var field_name = params[0];
				var source = document.querySelector('[name=' + field_name + ']');
				if(this.item.value != source.value)
				{
					var message = this.field.querySelector(".error-message");
					message.innerHTML = "Les deux mots de passe doivent être égaux";
					this.field.classList.add("error");
				}
			}
		});
	}
};

class FieldFactory extends Factory
{
	constructor()
	{
		super('[data-rules]');
	}

	build(item)
	{
		return new FieldController(item);
	}
}

export class FormController extends Controller
{
	constructor(item)
	{
		super(item);

		let factory = new FieldFactory();
		this.controllers = factory.buildAll();

		item.submit = (e) => this.submit(e);
	}

	submit(e)
	{
		let data = new FormData(this.item);
		return Controller.post(this.item.action, data);
	};
}

export class FormFactory extends Factory
{
	constructor()
	{
		super('form');
	}

	build(item)
	{
		return new FormController(item);
	}
}