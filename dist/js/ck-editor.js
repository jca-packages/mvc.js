
import { Controller, Factory } from "./controller.js";

export class CkEditorController extends Controller
{
	constructor(item)
	{
		super(item);

		hljs.configure({   // optionally configure hljs
			languages: ['javascript', 'ruby', 'python']
		});

		let quill = new Quill(item, {
			theme: 'snow',
			modules: 
			{
				syntax: {
					highlight: (text) => hljs.highlightAuto(text).value,
				},   
				toolbar: '#' + item.parentNode.id + ' .toolbar-container',
			},
		});

		quill.on('text-change', function(delta, oldDelta, source) {
			document.getElementById(item.dataset.target).querySelector('input[type=hidden]').value = JSON.stringify(quill.getContents());
		});
	}
}

export class CkEditorFactory extends Factory
{
	constructor()
	{
		super('.ck-textarea');
	}

	build(item)
	{
		return new CkEditorController(item);
	}
}