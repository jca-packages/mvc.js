import { Controller, Factory } from "./controller.js";

export class AppController extends Controller
{
	constructor(item)
	{
		super(item);

		this.factories = [];

		// innerHTML Observer
		const observer = new MutationObserver(mutationsList => {
			mutationsList.forEach(mutation => {
				
				mutation.addedNodes.forEach(node => this.contentAdded(node));
			});
		});
		observer.observe(item, {
			childList: true,
			subtree: true
		});

		item.onclick = (e) => this.click(e);
	}

	contentAdded(node)
	{
		if(!['#text', '#comment'].includes(node.nodeName))
			this.run(node);
	}

	click(e)
	{
		e = e || window.event;

		let element = e.target || e.srcElement;
		
		// Generi button
		if (element.tagName == 'BUTTON')
		{
			let button = element;
			let method = button.dataset.method;
		}

		// Submit button
		if (element.tagName == 'INPUT' && element.type == 'submit' && element.form.action != "")
		{
			//for(var i in CKEDITOR.instances) 
			//	CKEDITOR.instances[i].updateElement();

			let input = element;
			var data = new FormData(input.form);

			var submited = new Event("submit");

			let _this = this;

			Controller.post(input.form.action, data).then(function(response)
			{
				input.form.dispatchEvent(submited);

				_this.render(input.form.action, response);

				// URL Replace
				var historyState = {url: window.location.pathname};
				history.pushState(historyState, 'url', input.href);
			});

			e.preventDefault();
			e.stopPropagation();
		}

		// Link
		if (element.tagName != 'A')
			element = element.closest('a');
		if(element != null)
		{
			if(!element.href.startsWith(Controller.root))
				return true;
			
			if(element.hash.startsWith('#'))
				return true;

			if(element.href.endsWith(".pdf"))
				return true;
			
			let page = Controller.get(element.href);
			page.then(data =>
			{
				this.render(element.href, data);

				// URL Replace
				var historyState = {url: window.location.pathname};
				history.pushState(historyState, 'url', element.href);
			});

			e.preventDefault();
			e.stopPropagation();
		}
	}	

	run(root)
	{
		this.factories.forEach(factory => {
			factory.buildAll(root);
		});
	}

	render(route, data)
	{
		let newDoc = Controller.parser.parseFromString(data, 'text/html');
		let contents = newDoc.querySelectorAll('.content');
		contents.forEach(content => 
		{
			document.getElementById(content.id).innerHTML = content.innerHTML;	
		});

		// Dynamic JS load
		route = route.split(Controller.root + '/')[1];
		route = route.split('?')[0];
		this.loadScript(Controller.root + "/public/assets/mods/" + route.replace('/', '_') + '.js' + '?v=' + Date.now()).then(function(response)
		{
			
		});

		//this.run(data);
	}

	loadScript(url)
	{
		return new Promise(function (resolve, reject)
		{
			var script = document.createElement('script');
			script.addEventListener('load', function ()
			{
				resolve(script);
			});
			script.src = url;
			document.body.appendChild(script);
		});
	};
}

export class AppFactory extends Factory
{
	constructor()
	{
		super('#app');
	}

	build(item)
	{
		return new AppController(item);
	}
}

/*if (element != null)
{
	if ( (element.tagName == 'INPUT' && ['radio', 'checkbox', 'file'].includes(element.type)) || element.tagName == 'LABEL')
		return true;
	if (element.tagName == 'INPUT' && element.type == 'submit' && element.form.action != "")
	{
		var data = new FormData(element.form);

		promise = Controller.post(element.form.action, data).then(function(response)
		{
			element.form.dispatchEvent(formSubmitedEvent);

			_this.goto(element.form.action, response);

			// URL Replace
			var historyState = {url: window.location.pathname};
			history.pushState(historyState, 'url', element.href);
		});
	}
	else
	{
		

		if (element.tagName != 'A')
			element = element.closest('a');
		if(element != null)
		{
			if(!element.href.startsWith(Controller.root))
				return true;
			promise = Controller.get(element.href).then(function(response)
			{
				_this.goto(element.href, response);

				// URL Replace
				var historyState = {url: window.location.pathname};
				history.pushState(historyState, 'url', element.href);
			});
		}
	}

	return false;
}*/

/*var formSubmitedEvent = new Event("FormSubmited");

function AppController(item)
{
	Controller.call(this, item);

	var _this = this;

	

	this.loadScript = function (url)
	{
		return new Promise(function (resolve, reject)
		{
			var script = document.createElement('script');
			script.addEventListener('load', function ()
			{
				//resolve(true);
				resolve(script);
			});
			script.src = url;
			document.body.appendChild(script);
		});
	};

	this.goto = function(route, data)
	{
		var content_id = 'content';

		// Old content
		var oldContent = item.querySelector('#' + content_id);

		// New content
		var parser = new DOMParser();
		var htmlDoc = parser.parseFromString(data, 'text/html');

		// Content replace
		var newContent = htmlDoc.getElementById(content_id);
		if(newContent != null)
			oldContent.innerHTML = newContent.innerHTML;
		else
			oldContent.innerHTML = data;

		

		

		// Controllers refresh
		var contentLoaderEvent = new Event("ContentLoaded");
		contentLoaderEvent.item = oldContent;
		document.dispatchEvent(contentLoaderEvent);

		// Body classes
		var body_class = htmlDoc.getElementById('app').getAttribute('class');
		body_class = body_class.replace('preload', '');
		body_class = body_class.trim();
		item.setAttribute('class', body_class);
	}

	// TODO que pour index
	if(item.hasAttribute('data-root'))
	{
		var root = item.dataset.root;
		Controller.get(Controller.root + root).then(data =>
		{
			_this.goto(Controller.root + root, data);
			item.removeAttribute('data-root');
			
			// Start
			item.classList.remove("preload");
		});
	}
}

var contentLoadedEvent = new Event("ContentLoaded");
var contentAddedEvent = new Event("ContentAdded");

var app;

document.addEventListener('DOMContentLoaded', function()
{
	var item = document.querySelector('#app');
	app = new AppController(item);
}, false);

*/