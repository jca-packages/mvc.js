
import { Controller, Factory } from "./controller.js";

export class ModalController extends Controller
{
	static
	{
		const modalHTML = document.getElementById('modal').innerHTML;
		ModalController.modal = Controller.parser.parseFromString(modalHTML, 'text/html').body.childNodes[0];
		ModalController.modal.addEventListener('click', (e) => {
			if(e.target.id == 'modal_background')
				document.getElementById('app').removeChild(ModalController.modal);
		});
		ModalController.modal.querySelector('#modal_body').addEventListener('click', (e) => 
		{
			//e.preventDefault();
			//e.stopPropagation();
		});

		ModalController.open_event = new Event("open");
	}

	constructor(item)
	{
		super(item);

		item.open = () => this.open();
		item.hide = () => this.close();
	}

	open()
	{
		let body = ModalController.modal.querySelector('#modal_body');
		body.innerHTML = this.item.innerHTML;

		document.getElementById('app').appendChild(ModalController.modal);

		this.item.dispatchEvent(ModalController.open_event);
	}

	hide()
	{
		document.getElementById('app').removeChild(ModalController.modal);
	}
}

export class ModalFactory extends Factory
{
	constructor()
	{
		super('.modal');
	}

	build(item)
	{
		return new ModalController(item);
	}
}