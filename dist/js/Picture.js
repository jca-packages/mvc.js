
import { Controller, Factory } from "./controller.js";

export class PictureController extends Controller
{
	constructor(item)
	{
		super(item);

		new ResizeObserver(this.resize).observe(this.item);
	}

	resize(e)
	{
		let img = e[0].target.querySelector('img');

		if("src" in img.dataset && img.dataset.src.startsWith('holder.js/'))
		{
			let width = parseInt(e[0].contentRect.width);
			let height = img.dataset.src.split('x')[1];

			img.src = 'holder.js/' + width + 'x' + height;
			Holder.run(
			{
				images: img
			});
		}	
	}
}

export class PictureFactory extends Factory
{
	constructor()
	{
		super('.picture');
	}

	build(item)
	{
		return new PictureController(item);
	}
}