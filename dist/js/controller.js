export class Controller
{
	static root = location.origin;

	static parser = new DOMParser();

	constructor(item)
	{
		this.item = item;
		this.item.controller = this;
	}

	static async ajax(url, method, data)
	{
		let response = await fetch(url,
		{
			method: method,
			body: data
		});
		return await response.text();
	}

	static get(url)
	{
		return Controller.ajax(url, "get");
	}

	static post(url, data)
	{
		return Controller.ajax(url, "post", data);
	}
}

export class Factory
{
	constructor(accessor)
	{
		this.accessor = accessor
	}

	buildAll(root = document)
	{
		let controllers = [];

		let elements = Array.from(root.querySelectorAll(this.accessor));
		if(document != root && root.matches(this.accessor))
			elements.push(root);

		elements.forEach(element => {
			controllers.push(this.build(element));
		});

		return controllers;
	}
}