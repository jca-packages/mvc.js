const TIMEOUT_TIME = 1000 / 60;

function CaptureController(item)
{
	Controller.call(this, item);

	var _this = this;

	this.constraints =
	{
		audio: false,
		video:
		{
			facingMode:  "environment" ,
			width: { ideal: 1920 },
			height: { ideal: 1080 }
		}
	};

	this.handleSuccess = (stream) =>
	{
		// First frame event
		this.item.addEventListener('loadeddata', (event) =>
		{
			setTimeout(this.tick, TIMEOUT_TIME);
		});

		// Video stream
		this.item.srcObject = stream;
	}

	// Frame process
	this.tick = async function()
	{
		if( _this.item.videoWidth > 0 && _this.item.videoHeight > 0 )
			if( _this.newFrame != undefined )
				_this.newFrame();
		setTimeout(_this.tick, TIMEOUT_TIME);
	}

	item.setAttribute('autoplay', '');
	item.setAttribute('playsinline', '');
	item.setAttribute('muted', '');

	navigator.mediaDevices
		.getUserMedia(this.constraints)
		.then(this.handleSuccess);
}

document.addEventListener('DOMContentLoaded', function()
{
	var items = document.querySelectorAll('.capture');
	items.forEach(item =>
	{
		new CaptureController(item);
	});
}, false);
