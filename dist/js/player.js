function PlayerController(item)
{
	Controller.call(this, item);

	var _this = this;

	item.onclick = function (event)
	{
		var target = event.currentTarget;
		var icon = target.querySelector('.fa');
		if(icon.classList.contains("fa-play"))
		{
			icon.classList.remove("fa-play");
			icon.classList.add("fa-pause");
			_this.run();
		}
		else
		{
			icon.classList.remove("fa-pause");
			icon.classList.add("fa-play");
		}
	};

	this.stop = function ()
	{
		var span = item.querySelector('span');
		span.classList.remove("fa-pause");
		span.classList.add("fa-play");
	}
}
