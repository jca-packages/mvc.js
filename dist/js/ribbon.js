function RibbonController(item)
{
	Controller.call(this, item);

	this.page = 0;

	var scroll  = item.getAttribute('data-scroll');
	var template = item.getAttribute('data-template');

	this.addBrick = function(brick)
	{
		var columns = this.item.querySelectorAll('.column');
		var column = columns[0];
		for(i = 1; i < columns.length; i++)
			if(columns[i].childNodes.length < column.childNodes.length)
				column = columns[i];
		item.appendChild(brick);
	};

	this.getPage = function(page)
	{
		var queryString = window.location.search;
		url = Controller.root + '/' + this.route + (queryString.length > 0 ? queryString + '&': '?') + 'mode=json&page=' + this.page;

		if(item.hasAttribute('data-params'))
			url += '&' + item.getAttribute('data-params');
		Controller.get(url).then(data =>
		{
			var template_html = document.getElementById(template).innerHTML;

			for (var item of JSON.parse(data) )
			{
				var processed_template = process_template(template, template_html, item);

				var brick = new DOMParser().parseFromString(processed_template, 'text/html').body.childNodes[0];

				_this.addBrick(brick);

				var contentAddedEvent = new Event("ContentAdded");
				contentAddedEvent.item = brick;
				document.dispatchEvent(contentAddedEvent);
			}

			this.page++;
		});
	}

	var _this = this;

	this.route = item.getAttribute('data-scroll');

	this.item.addEventListener('scroll', (event) =>
	{
		const
		{
			scrollTop,
			scrollHeight,
			clientHeight
		} = event.target;

		if (scrollTop + clientHeight >= scrollHeight - 5)
		{
			this.getPage();
		}
	});

	this.getPage();
}

RibbonController.bind = function(root)
{
	var items = root.querySelectorAll('.ribbon');
	items.forEach(item =>
	{
		new RibbonController(item);
	});
}

document.addEventListener('DOMContentLoaded', function()
{
	RibbonController.bind(document);
}, false);

document.addEventListener('ContentLoaded', function(event)
{
	RibbonController.bind(event.item);
});
