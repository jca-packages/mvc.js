
import { Controller, Factory } from "./controller.js";

export class HolderController extends Controller
{
	constructor(item)
	{
		super(item);

		let _item = item;

		// Holder.js
		Holder.run(
        {
            images: item
        });
	}
}

export class HolderFactory extends Factory
{
	constructor()
	{
		super('img');
	}

	build(item)
	{
		return new HolderController(item);
	}
}